/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanadon.shape;

/**
 *
 * @author Acer
 */
public class TestShape {

    public static void main(String[] args) {
        Circle c1 = new Circle(5.2);
        Circle c2 = new Circle(4.5);
        Circle c3 = new Circle(1.5);
        Rectangle r1 = new Rectangle(3, 2);
        Rectangle r2 = new Rectangle(4, 3);
        Square s1 = new Square(2);
        Square s2 = new Square(4);

        Shape[] shapes = {c1, c2, c3, r1, r2, s1, s2};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i]);
            System.out.println(shapes[i].getName() + " area: " + shapes[i].calArea());
        }

    }
}
